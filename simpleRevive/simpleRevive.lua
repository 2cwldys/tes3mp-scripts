-- simpleRevive for tes3mp 0.7-prerelease. created by malic for JRP Roleplay
-- Activate a downed player to revive them wtih a percentage of their health
-- based on kanaRevive
-- under GPLv3

local simpleRevive = {}
simpleRevive.globalDownAlert = true -- true will alert entire server. false will only alert those in the same cell
simpleRevive.healthPercent = 20 -- how much of their max health players are revived with
simpleRevive.fatiguePercent = 20 -- how much of their max fatigue players are revived with
simpleRevive.magickaPercent = 20 -- how much of their max magicka players are revived with
config.deathTime = 15 -- overwrites config.lua entry

Player = require("player.json")
function Player:ProcessDeath() -- overwrites the original
	if config.playersRespawn then
		self.resurrectTimerId = tes3mp.CreateTimerEx("OnDeathTimeExpiration",time.seconds(config.deathTime), "i", self.pid)
		tes3mp.StartTimer(self.resurrectTimerId)
	else
		tes3mp.SendMessage(self.pid, "You have died permanently.", false)
	end
end

local function isDowned(pid)
	return simpleRevive[pid].isDead
end

local function ClearTable(eventStatus,pid)
	simpleRevive[pid] = {}
	simpleRevive[pid].isDead = false
	--simpleRevive[pid].preMagicka = 0 -- to give player back their previous magicka. currently not implemented
end

function OnObjectActivateValidator(eventStatus, pid, cellDescription, objects, players)
	for index = 0, tes3mp.GetObjectListSize() - 1 do
		local object={}
		local isObjectPlayer = tes3mp.IsObjectPlayer(index)
	
		if isObjectPlayer then
			targetPid = tes3mp.GetObjectPid(index)
			if isDowned(targetPid) then
				local newHealth = (simpleRevive.healthPercent / 100) * Players[targetPid].data.stats.healthBase
				local newFatigue =(simpleRevive.fatiguePercent / 100) * Players[targetPid].data.stats.fatigueBase
				local newMagicka = (simpleRevive.magickaPercent / 100) * Players[targetPid].data.stats.magickaBase
				tes3mp.LogMessage(enumerations.log.INFO, "[simpleRevive] " .. Players[pid].name .. " revived " .. Players[targetPid].name)
				Players[pid]:Message(color.SlateGray .. "You revived " .. Players[targetPid].name .. "\n" .. color.Default)
				Players[targetPid]:Message(color.SlateGray .. "You were revived by " .. Players[pid].name .. "\n" .. color.Default)
				simpleRevive[pid].isDead = false
				contentFixer.UnequipDeadlyItems(targetPid)
				tes3mp.Resurrect(targetPid, 0)
				tes3mp.SetHealthCurrent(targetPid, newHealth)
				tes3mp.SetFatigueCurrent(targetPid, newFatigue)
				tes3mp.SetMagickaCurrent(targetPid, newMagicka)
				tes3mp.SendStatsDynamic(targetPid)
				return customEventHooks.makeEventStatus(false, false) -- cancel any other activate scripts
			end			
		end
	end
end

local function OnDeathTimeExpirationValidator(eventStatus,pid)
	if not isDowned(pid) then -- if player was already revived, don't continue function
		return customEventHooks.makeEventStatus(false, false)
	end
	ClearTable(eventStatus,pid)
	local message = color.SlateGray .. Players[pid].name .. " (" .. pid .. ") bled out.\n" .. color.Default
	tes3mp.SendMessage(pid, message, true)
end

local function OnPlayerDeathHandler(eventStatus,pid)
	if not isDowned(pid) then -- if not meant to be dead, don't kill them
		return customEventHooks.makeEventStatus(false, nil)
	end
end

local function OnPlayerDeathValidator(eventStatus,pid)
	simpleRevive[pid].isDead = true -- set player as dead
	--simpleRevive[pid].preMagicka = tes3mp.GetMagickaCurrent(pid) -- save magicka value
	Players[pid]:Message(color.SlateGray .. "You were downed! You have " .. config.deathTime .. " seconds to be revived by another player." ..  color.Default .. "\n")
	
	if simpleRevive.globalDownAlert then
		local message = color.SlateGray .. Players[pid].name .. " (" .. pid .. ") was downed.\n"
		tes3mp.SendMessage(pid, message, true)
	else 
		local cellDescription = Players[pid].data.location.cell
		if logicHandler.IsCellLoaded(cellDescription) == true then
			for index, visitorPid in pairs(LoadedCells[cellDescription].visitors) do
				local message = color.SlateGray .. Players[pid].name .. " (" .. pid .. ") was downed.\n"
				tes3mp.SendMessage(visitorPid, message, false)
			end
		end
	end
end

customEventHooks.registerValidator("OnObjectActivate", OnObjectActivateValidator)
customEventHooks.registerValidator("OnPlayerDeath", OnPlayerDeathValidator)
customEventHooks.registerValidator("OnDeathTimeExpiration", OnDeathTimeExpirationValidator)
customEventHooks.registerHandler("OnPlayerDeath", OnPlayerDeathHandler)
customEventHooks.registerHandler("OnPlayerResurrect", ClearTable)
customEventHooks.registerHandler("OnPlayerAuthentified", ClearTable)
customEventHooks.registerHandler("OnPlayerDisconnect", ClearTable)
