## simpleRevive (for 0.7-alpha)
-	Activate a downed player to revive them wtih a percentage of their health
-	Replaces the default death messages
-	based on kanaRevive
-	Under GPLv3

### Installation
-	Add simpleRevive.lua to scripts/custom/
-	Add to customScripts.lua:
		`require("custom.simpleRevive")`

### To-do
-   Add setting to return magicka the player had upon being downed
