## jrpStatus (for 0.7)
-	A player-set dialogue other players can see when activating them
-	Made for roleplay purposes, inspired by WoW addons
-	Under GPLv3

![preview](preview.jpg)

### Installation
-	Add jrpStatus.lua to scripts/custom/
-	Add to customScripts.lua:
		`require("custom.jrpStatus")`

### Commands
-   **/status -** View your own status dialogue
-   **/status x texthere -** Set your text for that variable

### To-do
-   Injury display, GTA RP/SS13 style
-	GUI editor
-	Config options
