-- jrpRolls for tes3mp 0.7-prerelease. created by malic for JRP Roleplay
-- players choose their proficiency and buffs with d100 rolls
-- under GPLv3

local jrpRolls = {}

local skillArray = {"strength", "intelligence", "willpower", "agility", "endurance", "speed", "personality", "luck", 
"block", "armorer", "mediumarmor", "heavyarmor", "blunt", "longblade", "axe", "spear", "athletics",
"enchant", "destruction", "alteration", "illusion", "conjuration", "mysticism", "restoration", "alchemy", "unarmored",
"security", "sneak", "acrobatics", "lightarmor", "shortblade", "marksman", "mercantile", "speechcraft", "handtohand"}

local statsGUI = {}
statsGUI.mainSelect = 31320
statsGUI.attributeSelect = 31321
statsGUI.weaponSelect = 31322
statsGUI.armorSelect = 31323
statsGUI.magicSelect = 31324
statsGUI.fitnessSelect = 31325
statsGUI.socialSelect = 31326
statsGUI.stealthSelect = 31327
statsGUI.specificSelect = 31329

-- from based uramer
function jrpRolls.getRandom()
    math.random()
    math.random()
    math.random()
    return math.random()
end

function jrpRolls.round(num, numDecimalPlaces)
  local mult = 10^(numDecimalPlaces or 0)
  return math.floor(num * mult + 0.5) / mult
end

function jrpRolls.setSkill(eventStatus,pid,cmd)
	if cmd[2] ~= nil and cmd[3] ~= nil and cmd[4] ~= nil then
		local targetPid = tonumber(cmd[2])
		local targetName =  logicHandler.GetChatName(targetPid)
		local found = false
		local selectedskill = cmd[3]
		local newvalue = cmd[4]
		
		if found == false then
			for _,skill in pairs(skillArray) do
				if Players[targetPid].data.customVariables.jrpRolls[selectedskill] ~= nil then
					found = true
				end
			end
		end
		
		if found == true then
			Players[targetPid].data.customVariables.jrpRolls[selectedskill] = newvalue
			Players[pid]:Save()
			tes3mp.LogMessage(2, "[jrpRolls] " .. Players[pid].name .. " set " .. targetName .. "'s " .. selectedskill .. " to " .. Players[targetPid].data.customVariables.jrpRolls[selectedskill])
		end
	end
end

-- modified from Skvysh's rpRolls
function jrpRolls.DoRoll(eventStatus,pid,cmd)
    local cellDescription = Players[pid].data.location.cell
    local playerName = logicHandler.GetChatName(pid)
	local rollCheck = cmd[2]
    local message
    local roll
    local found = false
    
	if found == false then
		for _,skill in ipairs(skillArray) do
			if rollCheck == skill then
				found = true
            end
        end
	end
	
    if found == true then
         roll = jrpRolls.round((jrpRolls.getRandom()*100))
       
        if roll > 100 then
            roll = 100
        end
                
       message = color.BlueViolet .. playerName .. " rolled " .. (roll+Players[pid].data.customVariables.jrpRolls[rollCheck]) .. " (" .. roll .. "+" .. Players[pid].data.customVariables.jrpRolls[rollCheck] .. ") on " .. rollCheck .. " check.\n" .. color.Default
        if logicHandler.IsCellLoaded(cellDescription) == true then
            for index, visitorPid in pairs(LoadedCells[cellDescription].visitors) do
                tes3mp.SendMessage(visitorPid, message, false)
            end
        end
    else
        message = color.BlueViolet .. "Invalid attribute/skill.\n" .. color.Default
        tes3mp.SendMessage(pid, message, false)
    end
end

function jrpRolls.showMainSelect(eventStatus,pid)
	local message = color.Orange .. "jrpRolls - roll buff select" .. "\n" ..
	color.Yellow .. "selections made here will buff your /roll outcome" .. "\n" ..
	color.Red .. "other players can see your buffs, powergaming will result in a ban" .. "\n\n"
	tes3mp.CustomMessageBox(pid, statsGUI.mainSelect, message, "Attributes;Weapons;Armor;Magic;Fitness;Social;Stealth;Close")
end

function jrpRolls.showAttributeSelect(eventStatus,pid)
	local message = color.Orange .. "jrpRolls - attributes" .. "\n\n" ..
	color.Orange .. "Endurance: " .. color.Default ..  Players[pid].data.customVariables.jrpRolls.endurance .. "\n" ..
	color.Orange .. "Strength: " .. color.Default ..  Players[pid].data.customVariables.jrpRolls.strength .. "\n" ..
	color.Orange .. "Agility: " .. color.Default ..  Players[pid].data.customVariables.jrpRolls.agility .. "\n" ..
	color.Orange .. "Speed: " .. color.Default ..  Players[pid].data.customVariables.jrpRolls.speed .. "\n" ..
	color.Orange .. "Personality: " .. color.Default ..  Players[pid].data.customVariables.jrpRolls.personality .. "\n" ..
	color.Orange .. "Intelligence: " .. color.Default ..  Players[pid].data.customVariables.jrpRolls.intelligence .. "\n" ..
	color.Orange .. "Willpower: " .. color.Default ..  Players[pid].data.customVariables.jrpRolls.willpower .. "\n" ..
	color.Orange .. "Luck: " .. color.Default ..  Players[pid].data.customVariables.jrpRolls.luck .. "\n"
	tes3mp.CustomMessageBox(pid, statsGUI.attributeSelect, message, "Endurance;Strength;Agility;Speed;Personality;Intelligence;Willpower;Luck;Back")
end

function jrpRolls.showWeaponSelect(eventStatus,pid)
	local message = color.Orange .. "jrpRolls - weapons" .. "\n\n" ..
	color.Orange .. "Axe: " .. color.Default ..  Players[pid].data.customVariables.jrpRolls.axe .. "\n" ..
	color.Orange .. "Block: " .. color.Default ..  Players[pid].data.customVariables.jrpRolls.block .. "\n" ..
	color.Orange .. "Blunt Weapon: " .. color.Default ..  Players[pid].data.customVariables.jrpRolls.blunt .. "\n" ..
	color.Orange .. "Long Blade: " .. color.Default ..  Players[pid].data.customVariables.jrpRolls.longblade .. "\n" ..
	color.Orange .. "Marksman: " .. color.Default ..  Players[pid].data.customVariables.jrpRolls.marksman .. "\n" ..
	color.Orange .. "Short Blade: " .. color.Default ..  Players[pid].data.customVariables.jrpRolls.shortblade .. "\n" ..
	color.Orange .. "Spear: " .. color.Default ..  Players[pid].data.customVariables.jrpRolls.spear .. "\n"
	tes3mp.CustomMessageBox(pid, statsGUI.weaponSelect, message, "Axe;Block;Blunt Weapon;Long Blade;Marksman;Short Blade;Spear;Back")
end

function jrpRolls.showArmorSelect(eventStatus,pid)
	local message = color.Orange .. "jrpRolls - armor" .. "\n\n" ..
	color.Orange .. "Unarmored: " .. color.Default ..  Players[pid].data.customVariables.jrpRolls.unarmored .. "\n" ..
	color.Orange .. "Light Armor: " .. color.Default ..  Players[pid].data.customVariables.jrpRolls.lightarmor .. "\n" ..
	color.Orange .. "Medium Armor: " .. color.Default ..  Players[pid].data.customVariables.jrpRolls.mediumarmor .. "\n" ..
	color.Orange .. "Heavy Armor: " .. color.Default ..  Players[pid].data.customVariables.jrpRolls.heavyarmor .. "\n"
	tes3mp.CustomMessageBox(pid, statsGUI.armorSelect, message, "Unarmored;Light Armor;Medium Armor;Heavy Armor;Back")
end

function jrpRolls.showMagicSelect(eventStatus,pid)
	local message = color.Orange .. "jrpRolls - magic" .. "\n\n" ..
	color.Orange .. "Alchemy: " .. color.Default ..  Players[pid].data.customVariables.jrpRolls.alchemy .. "\n" ..
	color.Orange .. "Alteration: " .. color.Default ..  Players[pid].data.customVariables.jrpRolls.alteration .. "\n" ..
	color.Orange .. "Conjuration: " .. color.Default ..  Players[pid].data.customVariables.jrpRolls.conjuration .. "\n" ..
	color.Orange .. "Destruction: " .. color.Default ..  Players[pid].data.customVariables.jrpRolls.destruction .. "\n" ..
	color.Orange .. "Enchant: " .. color.Default ..  Players[pid].data.customVariables.jrpRolls.enchant .. "\n" ..
	color.Orange .. "Illusion: " .. color.Default ..  Players[pid].data.customVariables.jrpRolls.illusion .. "\n" ..
	color.Orange .. "Mysticism: " .. color.Default ..  Players[pid].data.customVariables.jrpRolls.mysticism .. "\n" ..
	color.Orange .. "Restoration: " .. color.Default ..  Players[pid].data.customVariables.jrpRolls.restoration .. "\n"
	tes3mp.CustomMessageBox(pid, statsGUI.magicSelect, message, "Alchemy;Alteration;Conjuration;Destruction;Enchant;Illusion;Mysticism;Restoration;Back")
end

function jrpRolls.showFitnessSelect(eventStatus,pid)
	local message = color.Orange .. "jrpRolls - fitness" .. "\n\n" ..
	color.Orange .. "Acrobatics: " .. color.Default ..  Players[pid].data.customVariables.jrpRolls.acrobatics .. "\n" ..
	color.Orange .. "Athletics: " .. color.Default ..  Players[pid].data.customVariables.jrpRolls.athletics .. "\n" ..
	color.Orange .. "Armorer: " .. color.Default ..  Players[pid].data.customVariables.jrpRolls.armorer .. "\n" ..
	color.Orange .. "Hand to Hand: " .. color.Default ..  Players[pid].data.customVariables.jrpRolls.handtohand .. "\n"
	tes3mp.CustomMessageBox(pid, statsGUI.fitnessSelect, message, "Acrobatics;Athletics;Armorer;Hand to Hand;Back")
end

function jrpRolls.showStealthSelect(eventStatus,pid)
	local message = color.Orange .. "jrpRolls - stealth" .. "\n\n" ..
	color.Orange .. "Security: " .. color.Default ..  Players[pid].data.customVariables.jrpRolls.security .. "\n" ..
	color.Orange .. "Sneak: " .. color.Default ..  Players[pid].data.customVariables.jrpRolls.sneak .. "\n"
	tes3mp.CustomMessageBox(pid, statsGUI.stealthSelect, message, "Security;Sneak;Back")
end

function jrpRolls.showSocialSelect(eventStatus,pid)
	local message = color.Orange .. "jrpRolls - social" .. "\n\n" ..
	color.Orange .. "Mercantile: " .. color.Default ..  Players[pid].data.customVariables.jrpRolls.mercantile .. "\n" ..
	color.Orange .. "Speechcraft: " .. color.Default ..  Players[pid].data.customVariables.jrpRolls.speechcraft .. "\n"
	tes3mp.CustomMessageBox(pid, statsGUI.socialSelect, message, "Mercantile;Speechcraft;Back")
end

function jrpRolls.showSpecificSelect(eventStatus,pid,selectedskill)
	local message = color.Orange .. "jrpRolls - select proficiency in " .. selectedskill .. "\n\n"
	tes3mp.CustomMessageBox(pid, statsGUI.specificSelect, message, "Hopeless;Clueless;Unlucky;Untrained (Default);Trained;Adept;Master;Back")
end

function jrpRolls.checkGUI(newStatus,pid,idGui,data)
	if idGui == statsGUI.mainSelect then
		if tonumber(data) == 0 then -- attribute
			jrpRolls.showAttributeSelect(eventStatus,pid)
		elseif tonumber(data) == 1 then -- weapon
			jrpRolls.showWeaponSelect(eventStatus,pid)
		elseif tonumber(data) == 2 then -- armor
			jrpRolls.showArmorSelect(eventStatus,pid)
		elseif tonumber(data) == 3 then -- magic
			jrpRolls.showMagicSelect(eventStatus,pid)
		elseif tonumber(data) == 4 then -- fitness
			jrpRolls.showFitnessSelect(eventStatus,pid)
		elseif tonumber(data) == 5 then -- social
			jrpRolls.showSocialSelect(eventStatus,pid)
		elseif tonumber(data) == 6 then -- stealth
			jrpRolls.showStealthSelect(eventStatus,pid)
		end
	elseif idGui == statsGUI.attributeSelect then
		if tonumber(data) == 0 then -- endurance
			selectedskill = "endurance"
			jrpRolls.showSpecificSelect(eventStatus,pid,selectedskill)
		elseif tonumber(data) == 1 then -- strength
			selectedskill = "strength"
			jrpRolls.showSpecificSelect(eventStatus,pid,selectedskill)
		elseif tonumber(data) == 2 then -- agility
			selectedskill = "agility"
			jrpRolls.showSpecificSelect(eventStatus,pid,selectedskill)
		elseif tonumber(data) == 3 then -- speed
			selectedskill = "speed"
			jrpRolls.showSpecificSelect(eventStatus,pid,selectedskill)
		elseif tonumber(data) == 4 then -- personality
			selectedskill = "personality"
			jrpRolls.showSpecificSelect(eventStatus,pid,selectedskill)
		elseif tonumber(data) == 5 then -- intelligence
			selectedskill = "intelligence"
			jrpRolls.showSpecificSelect(eventStatus,pid,selectedskill)
		elseif tonumber(data) == 6 then -- willpower
			selectedskill = "willpower"
			jrpRolls.showSpecificSelect(eventStatus,pid,selectedskill)
		elseif tonumber(data) == 7 then -- luck
			selectedskill = "luck"
			jrpRolls.showSpecificSelect(eventStatus,pid,selectedskill)
		elseif tonumber(data) == 8 then -- back
			jrpRolls.showMainSelect(eventStatus,pid)
		end
	elseif idGui == statsGUI.weaponSelect then
		if tonumber(data) == 0 then -- axe
			selectedskill = "axe"
			jrpRolls.showSpecificSelect(eventStatus,pid,selectedskill)
		elseif tonumber(data) == 1 then -- block
			selectedskill = "block"
			jrpRolls.showSpecificSelect(eventStatus,pid,selectedskill)
		elseif tonumber(data) == 2 then -- blunt weapon
			selectedskill = "blunt"
			jrpRolls.showSpecificSelect(eventStatus,pid,selectedskill)
		elseif tonumber(data) == 3 then -- long blade
			selectedskill = "longblade"
			jrpRolls.showSpecificSelect(eventStatus,pid,selectedskill)
		elseif tonumber(data) == 4 then -- marksman
			selectedskill = "marksman"
			jrpRolls.showSpecificSelect(eventStatus,pid,selectedskill)
		elseif tonumber(data) == 5 then -- short blade
			selectedskill = "shortblade"
			jrpRolls.showSpecificSelect(eventStatus,pid,selectedskill)
		elseif tonumber(data) == 6 then -- spear
			selectedskill = "spear"
			jrpRolls.showSpecificSelect(eventStatus,pid,selectedskill)
		elseif tonumber(data) == 7 then -- back
			jrpRolls.showMainSelect(eventStatus,pid)
		end
	elseif idGui == statsGUI.armorSelect then
		if tonumber(data) == 0 then -- unarmored
			selectedskill = "unarmored"
			jrpRolls.showSpecificSelect(eventStatus,pid,selectedskill)
		elseif tonumber(data) == 1 then -- light armor
			selectedskill = "lightarmor"
			jrpRolls.showSpecificSelect(eventStatus,pid,selectedskill)
		elseif tonumber(data) == 2 then -- medium armor
			selectedskill = "mediumarmor"
			jrpRolls.showSpecificSelect(eventStatus,pid,selectedskill)
		elseif tonumber(data) == 3 then -- heavy armor
			selectedskill = "heavyarmor"
			jrpRolls.showSpecificSelect(eventStatus,pid,selectedskill)
		elseif tonumber(data) == 4 then -- back
			jrpRolls.showMainSelect(eventStatus,pid)
		end
	elseif idGui == statsGUI.magicSelect then
		if tonumber(data) == 0 then -- alchemy
			selectedskill = "alchemy"
			jrpRolls.showSpecificSelect(eventStatus,pid,selectedskill)
		elseif tonumber(data) == 1 then -- alteration
			selectedskill = "alteration"
			jrpRolls.showSpecificSelect(eventStatus,pid,selectedskill)
		elseif tonumber(data) == 2 then -- conjuration
			selectedskill = "conjuration"
			jrpRolls.showSpecificSelect(eventStatus,pid,selectedskill)
		elseif tonumber(data) == 3 then -- destruction
			selectedskill = "destruction"
			jrpRolls.showSpecificSelect(eventStatus,pid,selectedskill)
		elseif tonumber(data) == 4 then -- enchant
			selectedskill = "enchant"
			jrpRolls.showSpecificSelect(eventStatus,pid,selectedskill)
		elseif tonumber(data) == 5 then -- illusion
			selectedskill = "illusion"
			jrpRolls.showSpecificSelect(eventStatus,pid,selectedskill)
		elseif tonumber(data) == 6 then -- mysticism
			selectedskill = "mysticism"
			jrpRolls.showSpecificSelect(eventStatus,pid,selectedskill)
		elseif tonumber(data) == 7 then -- restoration
			selectedskill = "restoration"
			jrpRolls.showSpecificSelect(eventStatus,pid,selectedskill)
		elseif tonumber(data) == 8 then -- back
			jrpRolls.showMainSelect(eventStatus,pid)
		end
	elseif idGui == statsGUI.fitnessSelect then
		if tonumber(data) == 0 then -- acrobatics
			selectedskill = "acrobatics"
			jrpRolls.showSpecificSelect(eventStatus,pid,selectedskill)
		elseif tonumber(data) == 1 then -- athletics
			selectedskill = "athletics"
			jrpRolls.showSpecificSelect(eventStatus,pid,selectedskill)
		elseif tonumber(data) == 2 then -- armorer
			selectedskill = "armorer"
			jrpRolls.showSpecificSelect(eventStatus,pid,selectedskill)
		elseif tonumber(data) == 3 then -- hand to hand
			selectedskill = "hand to hand"
			jrpRolls.showSpecificSelect(eventStatus,pid,selectedskill)
		elseif tonumber(data) == 4 then -- back
			jrpRolls.showMainSelect(eventStatus,pid)
		end
	elseif idGui == statsGUI.socialSelect then
		if tonumber(data) == 0 then -- mercantile
			selectedskill = "mercantile"
			jrpRolls.showSpecificSelect(eventStatus,pid,selectedskill)
		elseif tonumber(data) == 1 then -- speechcraft
			selectedskill = "speechcraft"
			jrpRolls.showSpecificSelect(eventStatus,pid,selectedskill)
		elseif tonumber(data) == 2 then -- back
			jrpRolls.showMainSelect(eventStatus,pid)
		end
	elseif idGui == statsGUI.stealthSelect then
		if tonumber(data) == 0 then -- security
			selectedskill = "security"
			jrpRolls.showSpecificSelect(eventStatus,pid,selectedskill)
		elseif tonumber(data) == 1 then -- sneak
			selectedskill = "sneak"
			jrpRolls.showSpecificSelect(eventStatus,pid,selectedskill)
		elseif tonumber(data) == 2 then -- back
			jrpRolls.showMainSelect(eventStatus,pid)
		end
	elseif idGui == statsGUI.specificSelect then
		if tonumber(data) == 0 then -- hopeless
			Players[pid].data.customVariables.jrpRolls[selectedskill] = -15
		elseif tonumber(data) == 1 then -- Clueless
			Players[pid].data.customVariables.jrpRolls[selectedskill] = -10
		elseif tonumber(data) == 2 then -- Unlucky
			Players[pid].data.customVariables.jrpRolls[selectedskill] = -5
		elseif tonumber(data) == 3 then -- Untrained
			Players[pid].data.customVariables.jrpRolls[selectedskill] = 0
		elseif tonumber(data) == 4 then -- Trained
			Players[pid].data.customVariables.jrpRolls[selectedskill] = 5
		elseif tonumber(data) == 5 then -- Adept
			Players[pid].data.customVariables.jrpRolls[selectedskill] = 10
		elseif tonumber(data) == 6 then -- Master
			Players[pid].data.customVariables.jrpRolls[selectedskill] = 15
		elseif tonumber(data) == 7 then -- Back
			jrpRolls.showMainSelect(eventStatus,pid)
		end
		Players[pid]:Save()
		jrpRolls.showMainSelect(eventStatus,pid)
		tes3mp.LogMessage(2, "[jrpRolls] " .. Players[pid].accountName .. " set their " .. selectedskill .. " to " .. Players[pid].data.customVariables.jrpRolls[selectedskill])
	end
end


function jrpRolls.ChatListener(pid, cmd)
	if cmd[1] == "setrollskill" and cmd[2] ~= nil and cmd[3] ~= nil and cmd[4] ~= nil and Players[pid]:IsAdmin() then
		jrpRolls.setSkill(eventStatus,pid,cmd)
	elseif cmd[1] == "skills" and cmd[2] == nil then
		jrpRolls.showMainSelect(eventStatus,pid)
	elseif cmd[1] == "roll" and cmd[2] ~= nil then
		jrpRolls.DoRoll(eventStatus,pid,cmd)
	else return
	end
end

function jrpRolls.onPlayerAuthentified(eventStatus, pid)
	if Players[pid].data.customVariables == nil then
			Players[pid].data.customVariables = {}
	end

	if Players[pid].data.customVariables.jrpRolls == nil then
			Players[pid].data.customVariables.jrpRolls = {}
	end
	
	math.randomseed(os.time()) -- preroll on authentification. ie, we only shuffle our deck once, and draw from it later.
	
	-- create jrpRolls.skill if it doesn't exist
	for _,skill in ipairs(skillArray) do
		if Players[pid].data.customVariables.jrpRolls[skill] == nil then
			Players[pid].data.customVariables.jrpRolls[skill] = 0
		end
	end
	
	Players[pid]:Save()
end

customCommandHooks.registerCommand("skills", jrpRolls.ChatListener)
customCommandHooks.registerCommand("setrollskill", jrpRolls.ChatListener)
customCommandHooks.registerCommand("roll", jrpRolls.ChatListener)
customEventHooks.registerValidator("OnGUIAction", jrpRolls.checkGUI)
customEventHooks.registerHandler("OnPlayerAuthentified", jrpRolls.onPlayerAuthentified)
