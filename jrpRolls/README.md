## jrpRolls (for 0.7-alpha)
-	A rolling script that lets players set their own roll bonuses
-	Roll bonuses can be set by admins
-	Under GPLv3

### Installation
-	Add jrpRolls.lua to scripts/custom/
-	Add to customScripts.lua:
		`require("custom.jrpRolls")`

### Commands
-   **/roll x -** Roll for a skill. ie, handtohand, enchant
-   **/skills -** GUI for players to set their bonuses
-	**/setrollskill pid skill num -** Admin command to set another players bonus

### To-do
-   Config option to disable self-setting
-	Config option to edit bonuses
-	Disallow numbers to go over 100 or under 0. Maybe set die sides to 100-buff?
-	Custom skills?
