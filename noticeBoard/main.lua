-- noticeBoard for tes3mp 0.7-prerelease. created by malic for JRP Roleplay
-- requires DataManager and CellInserter
-- under GPLv3

local noticeBoard = {}

noticeBoard.scriptName = "noticeBoard"
noticeBoard.defaultData = {}
noticeBoard.data = DataManager.loadData(noticeBoard.scriptName, noticeBoard.defaultData)

table.insert(guiHelper.names, "noticeboardMain")
table.insert(guiHelper.names, "noticeboardViewList")
table.insert(guiHelper.names, "noticeboardViewEntry")
table.insert(guiHelper.names, "noticeboardWrite")
guiHelper.ID = tableHelper.enum(guiHelper.names)

local function createRecord(refId,name,model)
    local recordStore = RecordStores["miscellaneous"]
    recordStore.data.permanentRecords[refId] = {
        name = name,
        model = model,
        script = "noPickUp"
    }
	recordStore:Save()
end

local function showMainGUI(eventStatus,pid)
	local message = color.Yellow .. "[ Public Notice Board ]\n" .. color.Gray .. "Entries are moderated"
	tes3mp.CustomMessageBox(pid, guiHelper.ID.noticeboardMain, message, "Write;View;Close")
end

local function addEntry(pid,data)
	local addedTime = os.date("%F-%I-%M-%S")
	noticeBoard.data[addedTime] = {}
	noticeBoard.data[addedTime]["content"] = tostring(data)
	noticeBoard.data[addedTime]["author"] = Players[pid].accountName
	table.sort(noticeBoard.data)
	DataManager.saveData("noticeBoard",noticeBoard.data)
end

local function showViewListGUI(eventStatus,pid)
	entriesByDate = {}
	local listPrefix = "* CLOSE *\n"
	for entryDate,entry in pairs(noticeBoard.data) do
		table.insert(entriesByDate,entryDate)
		table.sort(entriesByDate)
		list = listPrefix .. table.concat(entriesByDate,"\n")
	end
	local message = color.Yellow .. "[ Public Notice Board ]\n"
	tes3mp.ListBox(pid, guiHelper.ID.noticeboardViewList, message, list)
end

local function showViewEntryGUI(pid)
	tes3mp.LogMessage(2, "[noticeBoard] " .. Players[pid].name .. " is viewing entry " .. entryDate)
	local message = color.Yellow .. "[ Public Notice Board ]\n" .. color.Default .. "\n" .. entryContent .. "\n\n" .. entryAuthor .. " on " .. entryDate
	if Players[pid]:IsModerator() then
		buttons = "Delete;Close"
	else
		buttons = "Close"
	end
	tes3mp.CustomMessageBox(pid, guiHelper.ID.noticeboardViewEntry, message, buttons)
end

local function showWriteGUI(pid)
	local message = color.Gray .. "- Writing a public notice -\n"
	tes3mp.InputDialog(pid, guiHelper.ID.noticeboardWrite, message, "type blank to cancel")
end

local function CheckGUI(newStatus,pid,idGui,data)
	if idGui == guiHelper.ID.noticeboardMain then
		if tonumber(data) == 0 then -- write
			showWriteGUI(pid)
		elseif tonumber(data) == 1 then -- read
			showViewListGUI(eventStatus,pid)
		end
	elseif idGui == guiHelper.ID.noticeboardViewList then
		if tonumber(data) == 0 or tonumber(data) == 18446744073709551615 then --Close/Nothing Selected
			showMainGUI(eventStatus,pid)
		else
			local buttonPressed = tonumber(data)
			entryContent = noticeBoard.data[entriesByDate[buttonPressed]].content
			entryAuthor = noticeBoard.data[entriesByDate[buttonPressed]].author
			entryDate = entriesByDate[buttonPressed]
			showViewEntryGUI(pid)
		end
	elseif idGui == guiHelper.ID.noticeboardViewEntry then
		if Players[pid]:IsModerator() then
			if tonumber(data) == 0 then
				noticeBoard.data[entryDate] = nil
				DataManager.saveData("noticeBoard",noticeBoard.data)
				tes3mp.MessageBox(pid,-1,"You deleted a notice.")
				tes3mp.LogMessage(2, "[noticeBoard] " .. Players[pid].name .. " deleted entry " .. entryDate)
			elseif tonumber(data) == 1 then
				showViewListGUI(eventStatus,pid)
			end
		else
			if tonumber(data) == 0 then
				showViewListGUI(eventStatus,pid)
			end
		end
	elseif idGui == guiHelper.ID.noticeboardWrite then
		if data ~= nil and tostring(data) ~= "blank" then
			addEntry(pid,data)
			tes3mp.MessageBox(pid,-1,"You wrote a notice.")
		end
	end
end

local function OnObjectActivate(eventStatus, pid, cellDescription, objects, players)
    for _, object in pairs(objects) do
		if object.refId == "noticeboard_book" then
			showMainGUI(eventStatus,pid)
			return customEventHooks.makeEventStatus(false, false)
		end
    end
end


local function OnServerInit()
	createRecord("noticeboard_book","Notice Board","m\\Text_Quarto_open_01.NIF")
end


customEventHooks.registerValidator("OnGUIAction", CheckGUI)
customEventHooks.registerValidator("OnObjectActivate", OnObjectActivate)
customEventHooks.registerHandler("OnServerInit", OnServerInit)
