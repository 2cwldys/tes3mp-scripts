## noticeBoard (for 0.7-alpha)
-	A public book players can write and read posts from. Moderators can remove notices.
-	Place `noticeboard_book` to use
-	Requires DataManager
-	Under GPLv3

### Installation
-	Install [DataManager](https://github.com/tes3mp-scripts/DataManager)
-	Add main.lua to scripts/custom/noticeBoard/
-	Add to customScripts.lua somewhere under DataManager line:
		`require("custom.noticeBoard.main")`

### To-do
-	jrpChat/fake name support
-	Reverse listing order (if possible?)
