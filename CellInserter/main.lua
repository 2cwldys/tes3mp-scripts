-- CellInserter for tes3mp 0.7-prerelease, by Malic for JRP Roleplay
-- Requires DataManager by uramer
-- under GPLv3

local CellInserter = {}

CellInserter.scriptName = "CellInserter"
CellInserter.defaultConfig = require("custom.CellInserter.defaultConfig")
CellInserter.config = DataManager.loadConfiguration(CellInserter.scriptName, CellInserter.defaultConfig)
CellInserter.defaultData = DataManager.loadConfiguration(CellInserter.scriptName, CellInserter.defaultConfig)
CellInserter.data = DataManager.loadData(CellInserter.scriptName, CellInserter.defaultData)
CellInserter.cellDir = tes3mp.GetModDir() .. "/cell/"

function CellInserter.placeObject(pid,cellDescription,cell)
	for entryname,entry in pairs(cell) do
		if entryname ~= "insertCount" then
			tes3mp.LogMessage(2, "[CellInserter] Spawning " .. entry["refId"] .. " in " .. cellDescription)
			local location = {posX=entry["pos"][1],posY=entry["pos"][2],posZ=entry["pos"][3],rotX=entry["pos"][4],rotY=entry["pos"][5],rotZ=entry["pos"][6]}
			logicHandler.CreateObjectAtLocation(cellDescription,location,entry["refId"],"place")
		end
	end
end

function CellInserter.removeObject(pid,cellDescription,cell)
	for _,uniqueIndex in pairs(cell) do
		tes3mp.LogMessage(2, "[CellInserter] Removing " .. uniqueIndex .. " in " .. cellDescription)
		logicHandler.DeleteObjectForEveryone(cellDescription, uniqueIndex)
		LoadedCells[cellDescription].data.objectData[uniqueIndex] = nil
	end
end

function CellInserter.searchData(pid,cellDescription)
	for cellName,cell in pairs(CellInserter.data.insert) do
		if (cellName == cellDescription) then
			CellInserter.placeObject(pid,cellDescription,cell)
		end
	end
	
	for cellName,cell in pairs(CellInserter.data.remove) do
		if (cellName == cellDescription) then
			CellInserter.removeObject(pid,cellDescription,cell)
		end
	end
end

function CellInserter.addToData(cellDescription,refId,pos,scale)
	if CellInserter.data.insert[cellDescription] == nil then
		i = 1
		local iString = tostring(i)
		CellInserter.data.insert[cellDescription] = {}
		CellInserter.data.insert[cellDescription]["insertCount"] = 1
		CellInserter.data.insert[cellDescription][iString] = {}
		CellInserter.data.insert[cellDescription][iString]["refId"] = refId
		CellInserter.data.insert[cellDescription][iString]["pos"] = pos
		CellInserter.data.insert[cellDescription][iString]["scale"] = scale
	else
		i = CellInserter.data.insert[cellDescription].insertCount + 1
		local iString = tostring(i)
		CellInserter.data.insert[cellDescription].insertCount = i
		CellInserter.data.insert[cellDescription][iString] = {}
		CellInserter.data.insert[cellDescription][iString]["refId"] = refId
		CellInserter.data.insert[cellDescription][iString]["pos"] = pos
		CellInserter.data.insert[cellDescription][iString]["scale"] = scale
	end
	tempTable = {}
end

function CellInserter.isCellLoaded(eventStatus,pid,cellDescription)
	local cell = LoadedCells[cellDescription] -- load cell as table (i think)
	local cellFilePath = CellInserter.cellDir .. cell.entryFile
	
	if not tes3mp.DoesFileExist(cellFilePath) then -- if cell json doesn't exist, its our first load anyway
		tes3mp.LogMessage(2, "[CellInserter] Cell doesn't have json file, so placing objects after its been created")
		CellInserter.actOnCell(eventStatus,pid,cellDescription)
	elseif tableHelper.isEmpty(cell.data.objectData) then -- check for objectdata, for atkana cellreset
		tes3mp.LogMessage(2, "[CellInserter] Found cell with empty objectData. Placing objects assuming its been reset.")
		CellInserter.actOnCell(eventStatus,pid,cellDescription)
	end
end

function CellInserter.actOnCell(eventStatus,pid,cellDescription)
	CellInserter.searchData(pid,cellDescription)
	LoadedCells[cellDescription]:Save()
end

local function OnServerPostInit()
	tes3mp.LogMessage(2, "[CellInserter] initialized.")
end

local function ChatListener(pid,cmd)
	local cellDescription = tes3mp.GetCell(pid)
	
	if Players[pid]:IsAdmin() then
		if cmd[2] == nil or cmd[3] == nil then
			Players[pid]:Message(color.Gray .. "Command usage is: /ci insert/remove uniqueIndex" .. "\n" .. "uniqueIndex should look something like 11111-0 (ie. 87990-0)" .. "\n" .. color.Red .. "This feature has not been tested thoroughly. Objects may go missing.\n" .. color.Default)
		else
			local i = 0
			for uniqueIndex,object in pairs(LoadedCells[cellDescription].data.objectData) do
				if cmd[2] == "insert" and cmd[3] == uniqueIndex then
					local insert = CellInserter.config["insert"]
					
					local newTable = {
						["refId"] = object.refId,
						["pos"] = {object.location.posX,object.location.posY,object.location.posZ,object.location.rotX,object.location.rotY,object.location.rotZ},
						["scale"] = 1
					}
					
					if insert[cellDescription] == nil then
						i = 1
						local iString = tostring(i)
						CellInserter.config.insert[cellDescription] = {}
						CellInserter.config.insert[cellDescription]["insertCount"] = 1
						CellInserter.config.insert[cellDescription][iString] = newTable
					else
						i = CellInserter.config.insert[cellDescription].insertCount + 1
						local iString = tostring(i)
						CellInserter.config.insert[cellDescription].insertCount = i
						CellInserter.config.insert[cellDescription][iString] = newTable
					end
					DataManager.saveConfiguration("CellInserter",CellInserter.config) -- Saving to the config feels wrong, but with how the script is set up, it helps prevent object loss on data rewrite.
					Players[pid]:Message(color.Gray .. object.refId .. " in " .. cellDescription .. " saved to CellInserter.\n" .. color.Default)
				elseif cmd[2] == "remove" and cmd[3] == uniqueIndex then
					local remove = CellInserter.config["remove"]
					
					if remove[cellDescription] == nil then
						CellInserter.config.remove[cellDescription] = {}
						table.insert(CellInserter.config.remove[cellDescription], uniqueIndex)
					else
						table.insert(CellInserter.config.remove[cellDescription], uniqueIndex)
					end
					DataManager.saveConfiguration("CellInserter",CellInserter.config) -- Saving to the config feels wrong, but with how the script is set up, it helps prevent object loss on data rewrite.
					Players[pid]:Message(color.Gray .. uniqueIndex .. " in " .. cellDescription .. " set to delete in CellInserter.\n" .. color.Default)
				end
			end
		end
	end
end

local function OnServerExit()
	CellInserter.data = nil
	DataManager.saveData("CellInserter",CellInserter.data)
end

customCommandHooks.registerCommand("ci", ChatListener)
customEventHooks.registerHandler("OnCellLoad", CellInserter.isCellLoaded)
customEventHooks.registerHandler("OnServerPostInit", OnServerPostInit)
customEventHooks.registerHandler("OnServerExit", OnServerExit)

return CellInserter
