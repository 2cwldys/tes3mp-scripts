-- addon-esp for CellInserter for tes3mp 0.7-prerelease, by Malic for JRP Roleplay
-- Requires espParser by Jakob
-- NOTE: You probably don't want to run this on a production server. Expect performance hits.
-- Currently does not account for deletions.
-- under GPLv3

require("custom.CellInserter.main")
require("custom.espParser")

local doInfo = function(text)
    tes3mp.LogMessage(enumerations.log.INFO, text) 
end

doInfo("[CellInserter:addon-esp] Start")

for _, file in pairs(espParser.files) do    
	for cellName, cell in pairs(file.cells) do
		
		if cell.isExterior == true then
			cellDescription = cell.gridX .. ", " .. cell.gridY
		elseif cell.isExterior == false then
			cellDescription = cellName
		else
			return
		end
		
		for _, obj in pairs(cell.objects) do
			if obj.pos ~= nil then
				location = {obj.pos["XPos"],obj.pos["YPos"],obj.pos["ZPos"],obj.pos["XRot"],obj.pos["YRot"],obj.pos["ZRot"]}
			else
				location = {0,0,0,0,0,0}
			end
			
			CellInserter.addToData(cellDescription,obj.refId,location,obj.scale)
		end
	end
	DataManager.saveData("CellInserter",CellInserter.data)
end

doInfo("CellInserter:addon-esp] End")
