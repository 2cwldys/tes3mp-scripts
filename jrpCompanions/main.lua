-- jrpCompanions modified for JRP. For tes3mp 0.7-prerelease
-- thanks to RickOff, 1338, uramer, Davic C
-- forked and heavily modified from Ecarlate's compagnonScript.
-- Under GPLv3 license

--[[ install
requires DataManager (https://github.com/tes3mp-scripts/DataManager)

add to customScripts.lua:
	require("custom.jrpCompanions.main")
--]]

local companionList = {}
local jrpCompanions = {}
jrpCompanions.scriptName = "jrpCompanions"

jrpCompanions.defaultConfig = {
	companions = {
		scrib = {
			name = "scrib",
			refId = "scrib",
			price = 100,
			speed = 2,
			scale = 1
		}
	}
}

jrpCompanions.config = DataManager.loadConfiguration(jrpCompanions.scriptName, jrpCompanions.defaultConfig)

jrpCompanions.defaultData = {}
jrpCompanions.data = DataManager.loadData(jrpCompanions.scriptName, jrpCompanions.defaultData)

function jrpCompanions.CreateCompanion(pid, refId)
	if Players[pid]:IsLoggedIn() then
		local cellDescription = tes3mp.GetCell(pid)
		local location = {posX = (tes3mp.GetPosX(pid) + 150), posY = tes3mp.GetPosY(pid), posZ = tes3mp.GetPosZ(pid),rotX = 0, rotY = 0, rotZ = tes3mp.GetRotZ(pid) + 3.1}
			
		local uniqueIndex = logicHandler.CreateObjectAtLocation(cellDescription, location, refId, "spawn")
		logicHandler.SetAIForActor(LoadedCells[cellDescription], uniqueIndex, enumerations.ai.FOLLOW, pid)

		local accountName = Players[pid].accountName
		local data = jrpCompanions.data
		
		if jrpCompanions.data[accountName] == nil then
			jrpCompanions.data[accountName] = {}
		end
		
		table.insert(jrpCompanions.data[accountName], uniqueIndex)
		table.insert(jrpCompanions.data[accountName], cellDescription)
		table.insert(jrpCompanions.data[accountName], location)
		table.insert(jrpCompanions.data[accountName], "follow")
		
		DataManager.saveData(jrpCompanions.scriptName, jrpCompanions.data)
		
		tes3mp.LogMessage(enumerations.log.INFO, "[jrpCompanions] Creating companion " .. refId .. " for " .. accountName .. " at " .. cellDescription)
	end
end

function jrpCompanions.InstructCompanion(pid,cmd)
	local cellDescription = tes3mp.GetCell(pid)
	local accountName = Players[pid].accountName
	local uniqueIndex = jrpCompanions.data[accountName][1]
	jrpCompanions.guaranteeTable(pid)
	if uniqueIndex ~= nil and cmd[2] == "follow" then
		logicHandler.SetAIForActor(LoadedCells[cellDescription], uniqueIndex, enumerations.ai.FOLLOW, pid)
		jrpCompanions.data[accountName][4] = "follow"
	elseif uniqueIndex ~= nil and cmd[2] == "stay" then
		logicHandler.SetAIForActor(LoadedCells[cellDescription], uniqueIndex, enumerations.ai.CANCEL, pid)
		jrpCompanions.data[accountName][4] = "stay"
	end
	DataManager.saveData(jrpCompanions.scriptName, jrpCompanions.data)
end

function jrpCompanions.guaranteeTable(pid)
	if Players[pid].data.customVariables.jrpCompanions == nil then
		Players[pid].data.customVariables.jrpCompanions = {}
	end
end

-- generates list of companions names. for listing
function jrpCompanions.generateCompanionList()
	for companionFound in pairs(jrpCompanions.config.companions) do
			table.insert(companionList,companionFound)
	end
	tes3mp.LogMessage(2, "jrpCompanions initialized with: " .. table.concat(companionList,", "))
end

function jrpCompanions.printCompanionList(pid,cmd)
	Players[pid]:Message(color.Green .. "Available for purchase: " .. color.Grey .. table.concat(companionList,", ") .. ".\n" .. color.Default)
end

function jrpCompanions.printPlayerCompanionList(pid,cmd)
	jrpCompanions.guaranteeTable(pid)
	-- can't get this working for now
	--if Players[pid].data.customVariables.jrpCompanions.BoughtCompanions == {} then
	--	Players[pid]:Message(color.Gray .. "You don't own any companions. You can buy them using /companion shop.\n" .. color.Default)
	--else
		Players[pid]:Message(color.Orange .. "You can summon: " .. color.Grey .. table.concat(Players[pid].data.customVariables.jrpCompanions,", ") .. ".\n" .. color.Default)
	--end
end

local function getPlayerGold(pid)
	local goldLoc = inventoryHelper.getItemIndex(Players[pid].data.inventory, "gold_001", -1)
	
	if goldLoc then
		return Players[pid].data.inventory[goldLoc].count
	else
		return 0
	end
end

local function removeGold(pid, amount)
	local goldLoc = inventoryHelper.getItemIndex(Players[pid].data.inventory, "gold_001", -1)	
	Players[pid].data.inventory[goldLoc].count = Players[pid].data.inventory[goldLoc].count - amount
	
	local itemref = {refId = "gold_001", count = amount, charge = -1}			
	Players[pid]:Save()
	Players[pid]:LoadItemChanges({itemref}, enumerations.inventory.REMOVE)			
end

function jrpCompanions.vanishCompanion(pid,cmd)
	local accountName = Players[pid].accountName
	jrpCompanions.guaranteeTable(pid)
	if not tableHelper.isEmpty(jrpCompanions.data[accountName]) then
		tes3mp.LogMessage(enumerations.log.INFO, "[jrpCompanions] Vanishing current companion for " .. accountName)
		local cellDescription = jrpCompanions.data[accountName][2]
		local uniqueIndex = jrpCompanions.data[accountName][1]
					
		logicHandler.DeleteObjectForEveryone(cellDescription, uniqueIndex)
		LoadedCells[cellDescription].data.objectData[uniqueIndex] = nil
		LoadedCells[cellDescription]:Save()
					
		jrpCompanions.data[accountName] = {}
					
		DataManager.saveData(jrpCompanions.scriptName, jrpCompanions.data)
	end
end

-- used on call
function jrpCompanions.callCompanion(pid,cmd)
	jrpCompanions.guaranteeTable(pid)
	local accountName = Players[pid].accountName
	local desiredCompanion = cmd[3]
	for _,companion in pairs(companionList) do
		for _,ownedCompanion in pairs(Players[pid].data.customVariables.jrpCompanions) do
			if companion == desiredCompanion and desiredCompanion == ownedCompanion then
				local refId = jrpCompanions.config.companions[companion].refId
                jrpCompanions.vanishCompanion(pid,cmd)
				jrpCompanions.CreateCompanion(pid, refId)
			end
		end
	end
end

function jrpCompanions.buyCompanion(pid,cmd)
	local desiredCompanion = cmd[3]
	local pgold = getPlayerGold(pid)
	for _,companion in pairs(companionList) do
		if companion == desiredCompanion and cmd[4] == nil then
			Players[pid]:Message(color.Green .. "Would you like to buy a " .. color.DarkKhaki .. companion .. color.Green .. " for " .. color.Yellow .. jrpCompanions.config.companions[companion].price .. " gold?\n" .. color.Gray .. "Run command again, with \"confirm\" at the end\n" .. color.Default)
		elseif companion == desiredCompanion and cmd[4] == "confirm" then
			jrpCompanions.guaranteeTable(pid)
			if Players[pid].data.customVariables.jrpCompanions[companion] ~= nil then
				Players[pid]:Message(color.Red .. "You already own a " .. companion .. "!\n" .. color.Default)
			elseif Players[pid].data.customVariables.jrpCompanions[companion] == nil and pgold < jrpCompanions.config.companions[companion].price then
				Players[pid]:Message(color.Red .. "You can't afford a " .. companion .. "!\n" .. color.Default)
			else
				table.insert(Players[pid].data.customVariables.jrpCompanions,desiredCompanion)
				removeGold(pid,jrpCompanions.config.companions[companion].price)
				Players[pid]:Save()
				Players[pid]:Message(color.Green .. "You bought a " .. desiredCompanion .. "!\n" .. color.Default)
			end
		end
	end
end

-- for future functionality
function jrpCompanions.OnObjectActivateValidator(eventStatus, pid, cellDescription, objects, players)
    for _, object in pairs(objects) do
        if object.RefId == mpNum then
            tes3mp.LogMessage(2, "companion activated")
        end
    end
end

function jrpCompanions.ChatListener(pid, cmd)
	if cmd[2] == "follow" then
		jrpCompanions.InstructCompanion(pid,cmd)
	elseif cmd[2] == "stay" then
		jrpCompanions.InstructCompanion(pid,cmd)
	elseif cmd[2] == "vanish" then
		jrpCompanions.vanishCompanion(pid,cmd)
	elseif cmd[2] == "call" and cmd[3] ~= nil then
		jrpCompanions.callCompanion(pid,cmd)
	elseif cmd[2] == "buy" and cmd[3] == nil then
		jrpCompanions.printCompanionList(pid,cmd)
	elseif cmd[2] == "buy" and cmd[3] ~= nil then
		jrpCompanions.buyCompanion(pid,cmd)
	elseif cmd[2] == "list" then
		jrpCompanions.printPlayerCompanionList(pid,cmd)
	else return
	end
end

function jrpCompanions.OnPlayerAuthentified(eventStatus, pid)
	local accountName = Players[pid].accountName
	if Players[pid].data.customVariables == nil then
		Players[pid].data.customVariables = {}
	end

	if Players[pid].data.customVariables.jrpCompanions == nil then
		Players[pid].data.customVariables.jrpCompanions = {}
	end
	
	if jrpCompanions.data[accountName] == nil then
		jrpCompanions.data[accountName] = {}
		DataManager.saveData(jrpCompanions.scriptName, jrpCompanions.data)
	end
	
	Players[pid]:Save()
end

function jrpCompanions.OnPlayerDisconnect(eventStatus, pid)
	local cellDescription = tes3mp.GetCell(pid)
	local accountName = Players[pid].accountName
	local actorListSize = tes3mp.GetActorListSize()
	jrpCompanions.guaranteeTable(pid)
	for actorIndex = 0, actorListSize - 1 do
		local uniqueIndex = tes3mp.GetActorRefNum(actorIndex) .. "-" .. tes3mp.GetActorMpNum(actorIndex)
		
		if LoadedCells[cellDescription]:ContainsObject(uniqueIndex) and uniqueIndex == jrpCompanions.data[accountName][1] then
			logicHandler.DeleteObjectForEveryone(cellDescription, uniqueIndex)
			LoadedCells[cellDescription].data.objectData[jrpCompanions.data[accountName][1]] = nil
			LoadedCells[cellDescription]:Save()
						
			jrpCompanions.data[accountName] = {}
			DataManager.saveData(jrpCompanions.scriptName, jrpCompanions.data)
		end
	end
end

function jrpCompanions.OnActorDeath(eventStatus,pid,cellDescription)
	local cellDescription = tes3mp.GetCell(pid)
	local accountName = Players[pid].accountName
	local actorListSize = tes3mp.GetActorListSize()
	
	for actorIndex = 0, actorListSize - 1 do
		local uniqueIndex = tes3mp.GetActorRefNum(actorIndex) .. "-" .. tes3mp.GetActorMpNum(actorIndex)
		
		if LoadedCells[cellDescription]:ContainsObject(uniqueIndex) and uniqueIndex == jrpCompanions.data[accountName][1] then
			logicHandler.DeleteObjectForEveryone(cellDescription, uniqueIndex)
			LoadedCells[cellDescription].data.objectData[jrpCompanions.data[accountName][1]] = nil
			LoadedCells[cellDescription]:Save()
						
			jrpCompanions.data[accountName] = {}
			DataManager.saveData(jrpCompanions.scriptName, jrpCompanions.data)
		end
	end
end

function jrpCompanions.OnServerExit(eventStatus)
	jrpCompanions.data = {}
	DataManager.saveData(jrpCompanions.scriptName, jrpCompanions.data)
end

function jrpCompanions.OnServerPostInit()
	jrpCompanions.generateCompanionList()
	
	if jrpCompanions.data == nil then
		jrpCompanions.data = {}
	end
	
end

customCommandHooks.registerCommand("companion", jrpCompanions.ChatListener)
customEventHooks.registerHandler("OnPlayerAuthentified", jrpCompanions.OnPlayerAuthentified)
customEventHooks.registerHandler("OnServerPostInit", jrpCompanions.OnServerPostInit)
customEventHooks.registerHandler("OnServerExit", jrpCompanions.OnServerExit)
customEventHooks.registerHandler("OnActorDeath", jrpCompanions.OnActorDeath)
customEventHooks.registerValidator("OnPlayerDisconnect", jrpCompanions.OnPlayerDisconnect)
--customEventHooks.registerValidator("OnObjectActivate", jrpCompanions.OnObjectActivateValidator)
