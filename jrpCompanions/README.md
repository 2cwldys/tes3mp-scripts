## jrpCompanions (for 0.7-alpha)
-	A *.json* configurable companion/follower script
-	(Probably) no redundant functions!
-	Originally forked and heavily modified from [Ecarlate](https://github.com/rickoff/Tes3mp-Ecarlate-Script)'s compagnonScript
-	Under GPLv3

### Installation
-	Install [DataManager](https://github.com/tes3mp-scripts/DataManager) by uramer
-	Add main.lua to scripts/custom/jrpCompanions/
-	Add to customScripts.lua under DataManagers line:
		`jrpCompanions = require("custom.jrpCompanions.main")`

### Commands
-   **/companion buy -** Show companions available for purchase
-   **/companion buy x (confirm)-** Buy a companion
-   **/companion list -** Show all purchased companions
-   **/companion call x -** Summon a companion you own
-   **/companion follow -** Sets your current companion to follow you
-   **/companion stay -** Sets your current companion to stay in place
-   **/companion vanish -** Remove your current companion from the world

### To-do
-	Add speed/scale options per companion (butterfly and scrib are insanely slow)
-	Allow accessing companion inventory (ContainerFramework?)
-	Persistent inventory, leveling, and stats maybe?
-	Let companions follow into other cells, stay summoned between server resets
