-- jrpFixes for tes3mp 0.7-prerelease. created by malic for JRP Roleplay
-- under GPLv3

local jrpFixes = {}
jrpFixes.config = jsonInterface.load("custom/jrpFixes_config.json")

local function OnPlayerAuthentified(eventStatus,pid)
	for _,consoleInput in pairs(jrpFixes.config.console_pidOnLogin) do
		logicHandler.RunConsoleCommandOnPlayer(pid, consoleInput)
	end
end

local function OnObjectDelete(eventStatus,pid,cellDescription,objects)
	for _,cell in pairs(jrpFixes.config.general.disable_pickup) do
		if cellDescription == cell then
			for _, object in pairs(objects) do
				for _,excludeobject in pairs(jrpFixes.config.general.disable_pickup_exclude) do
					if object.refId ~= excludeobject then
						tes3mp.MessageBox(pid, -1, jrpFixes.config.general.disable_pickup_msg)
						return customEventHooks.makeEventStatus(false, false)
					end
				end
			end
		end
	end
end

local function OnCellLoadHandler(eventStatus,pid,cellDescription)
	for cellName,cell in pairs(jrpFixes.config.change_doorLock) do
		if cellName == cellDescription then
			local cellData = LoadedCells[cellDescription].data
			for uniqueIndex,state in pairs(jrpFixes.config.change_doorLock[cellName]) do
				if cellData.objectData[uniqueIndex] ~= nil then
					cellData.objectData[uniqueIndex].lockLevel = state
					
				else
					cellData.objectData[uniqueIndex] = {
						lockLevel = state,
						refId = "ex_nord_door_01"
					}
				end
				tes3mp.LogMessage(2, "[jrpFixes] " .. uniqueIndex .. " in " .. cellName .. " set to lock state " .. state)
				LoadedCells[cellDescription]:LoadObjectsLocked(pid,cellData.objectData,{uniqueIndex})
			end
		end
	end
end

-- inspired by skvysh's startupScripts
local function OnPlayerJournalHandler(eventStatus,pid)
	for _,questObj in pairs(Players[pid].data.journal) do
		if questObj.quest ~= nil and questObj.index ~= nil then
			for questId,configEntry in pairs(jrpFixes.config.quest) do
				if questObj.quest == questId and questObj.index == configEntry.state then
					tes3mp.LogMessage(2, "[jrpFixes] Setting " .. questId .. " from " .. configEntry.state .. " to " .. configEntry.newstate .. " for "  .. Players[pid].name)
					questObj.index = configEntry.newState -- Replacing journal entry to prevent redundant log entries
					Players[pid]:Save() -- there's probably a better way to do all of this (currently makes duplicate journal entry)
					logicHandler.RunConsoleCommandOnPlayer(pid, "journal " .. questId .. " " .. configEntry.newstate)
				end
			end
		end
	end
end

contentFixer.ValidateCellChange = function(pid)
	if jrpFixes.config.enable_census == true then
		return true
	end
end

--customEventHooks.registerValidator("OnObjectDelete", OnObjectDelete)
customEventHooks.registerHandler("OnCellLoad", OnCellLoadHandler)
customEventHooks.registerHandler("OnPlayerAuthentified", OnPlayerAuthentifiedHandler)
customEventHooks.registerHandler("OnServerPostInit", OnServerPostInitHandler)
customEventHooks.registerHandler("OnPlayerJournal", OnPlayerJournalHandler)
