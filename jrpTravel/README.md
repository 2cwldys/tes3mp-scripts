## jrpTravel (for 0.7-alpha)
-	Activatable object/NPCs to teleport between Mages Guilds/Telvanni cities without a map
-	Requires DataManager and CellInserter
-	Tamriel Rebuilt support
-	Under GPLv3

### Installation
-	Add main.lua to scripts/custom/jrpTravel/
-	Add to customScripts.lua under DataManager and CellInserters lines:
		`require("custom.jrpTravel.main")`

### To-do
-	Make prices relate to distance (currently a flat fee)
