-- setSex for tes3mp 0.7-prerelease. created by malic for JRP Roleplay
-- /setsex pid male/female
-- under BSD

local setSex = {}

function setSex.changeSex(pid,cmd)
	if logicHandler.CheckPlayerValidity(pid, cmd[2]) then
	
		local targetPid = tonumber(cmd[2])
        local newSex = tableHelper.concatenateFromIndex(cmd, 3)

        function setSexFunction()
			Players[targetPid].data.character.gender = newSex
			tes3mp.SetIsMale(targetPid, newSex)
			tes3mp.SetResetStats(targetPid, false)
			tes3mp.SendBaseInfo(targetPid)
		end
            
        if newSex == "male" or newSex == "1" then 
			newSex = 1
			setSexFunction()
		elseif newSex == "female" or newSex == "0" then
			newSex = 0
			setSexFunction()
		else 
			tes3mp.SendMessage(pid, "That is not an available sex. Use one of the following: male/1, female/0\n", false)
		end

	end
end

function setSex.ChatListener(pid, cmd)
	if cmd[1] == "setsex" and Players[pid]:IsAdmin() then
		setSex.changeSex(pid,cmd)
	else return
	end
end

customCommandHooks.registerCommand("setsex", setSex.ChatListener)
