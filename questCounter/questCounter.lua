-- questCounter for tes3mp 0.7-prerelease. created by malic for JRP Roleplay
-- under GPLv3

local questCounter = {}
questCounter.config = jsonInterface.load("custom/questCounter_config.json")

table.insert(guiHelper.names, "questCounter_main")
guiHelper.ID = tableHelper.enum(guiHelper.names)

local function round(num, numDecimalPlaces)
  local mult = 10^(numDecimalPlaces or 0)
  return math.floor(num * mult + 0.5) / mult
end

local function showMainGUI(eventStatus,pid)
	local message = color.Orange .. "[Quest Counter]\n"
	
	for factionID,faction in pairs(questCounter.config) do
		local completedQuests = 0
		local totalQuests = 0
		for questID,completeState in pairs(questCounter.config[factionID]) do
			totalQuests = totalQuests + 1
			
			if type(completeState) == "number" then
				for _,questObj in pairs(Players[pid].data.journal) do
					if questObj.quest ~= nil and questObj.quest == string.lower(questID) and questObj.index == completeState then
						completedQuests = completedQuests + 1
					end
				end
			elseif type(completeState) == "table" then
				for _,singleState in pairs(completeState) do
					for _,questObj in pairs(Players[pid].data.journal) do
						if questObj.quest ~= nil and questObj.quest == string.lower(questID) and questObj.index == singleState then
							completedQuests = completedQuests + 1
						end
				end
				end
			end
		end
		local completePercent = (completedQuests / totalQuests) * 100
		local completePercent = round(completePercent, 2)
		message = message .. color.Yellow .. factionID .. " (" .. completedQuests .. "/" .. totalQuests .. ") - " .. color.Default .. completePercent .. "%\n"
	end
	tes3mp.CustomMessageBox(pid, guiHelper.ID.questCounter_main, message, "Close")
end

local function ChatListener(pid,cmd)
	if cmd[1] == "quests" then
		showMainGUI(eventStatus,pid)
	end
end

customCommandHooks.registerCommand("quests", ChatListener)
