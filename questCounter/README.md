## questCounter (for 0.7-alpha)
-	Percentage tracker of how many quests a player has done in each region and faction
-	Under GPLv3

### Installation
-	Add questCounter_config.json to data/custom
-	Add questCounter.lua to scripts/custom/
-	Add to customScripts.lua
		`require("custom.questCounter")`

### To-do
-	Refine list (likely inaccurate)
-	Change Blades list to Backpath if certain quest is done?
-	Add other factions/misc quests
