-- jrpShop for tes3mp 0.7-prerelease. created by malic for JRP Roleplay
-- under GPLv3

jrpShop = {}

jrpShop.scriptName = "jrpShop"
jrpShop.defaultConfig = require("custom.jrpShop.defaultConfig")
jrpShop.config = DataManager.loadConfiguration(jrpShop.scriptName, jrpShop.defaultConfig)

table.insert(guiHelper.names, "jrpShop_mainMenu")
table.insert(guiHelper.names, "jrpShop_bodyMenu")
table.insert(guiHelper.names, "jrpShop_armorMenu")
table.insert(guiHelper.names, "jrpShop_clothingMenu")
table.insert(guiHelper.names, "jrpShop_heightMenu")
table.insert(guiHelper.names, "jrpShop_buy")
table.insert(guiHelper.names, "jrpShop_select")
guiHelper.ID = tableHelper.enum(guiHelper.names)

local shopTable = {} -- where we store each players options

local function ClearTable(pid)
	shopTable[pid] = {}
end

local function toggleTVM(pid)
	logicHandler.RunConsoleCommandOnPlayer(pid, "tvm") -- rotate camera around character
end

local function ShowMainMenu(eventStatus,pid)
	local message = color.Cyan .. "[ jrpShop ]"
	tes3mp.CustomMessageBox(pid, guiHelper.ID.jrpShop_mainMenu, message, "Change Body;Buy Armor;Buy Clothing;Buy Misc;Close")
end

local function ShowBodyMenu(eventStatus,pid)
	local message = color.Cyan .. "[ jrpShop - Body ]"
	tes3mp.CustomMessageBox(pid, guiHelper.ID.jrpShop_bodyMenu, message, "Change Hair;Change Head;Change Height;Back")
	ClearTable(pid)
end

local function ShowArmorMenu(eventStatus,pid)
	local message = color.Cyan .. "[ jrpShop - Armor ]"
	tes3mp.CustomMessageBox(pid, guiHelper.ID.jrpShop_armorMenu, message, "Headgear;Cuirass;Greaves;Left Pauldron;Right Pauldron;Left Gauntlet;Right Gauntlet;Boots;Shields;Back")
	ClearTable(pid)
end

local function ShowClothingMenu(eventStatus,pid)
	local message = color.Cyan .. "[ jrpShop - Clothing ]"
	tes3mp.CustomMessageBox(pid, guiHelper.ID.jrpShop_clothingMenu, message, "Shirts;Pants;Robes;Right Gloves;Left Gloves;Skirts;Shoes;Back")
	ClearTable(pid)
end

local function ShowHeightMenu(eventStatus,pid)
	local message = color.Cyan .. "[ jrpShop - Height ]\n" .. color.Default .. "Your current scale is " .. Players[pid].data.shapeshift.scale
	tes3mp.CustomMessageBox(pid, guiHelper.ID.jrpShop_heightMenu, message, "<-;Back;->")
end

local function ShowBuyMenu(eventStatus,pid)
	local message = color.Orange .. shopTable[pid].itemName .. "\n" .. color.Yellow .. shopTable[pid].itemCost .. " gold"
	tes3mp.CustomMessageBox(pid, guiHelper.ID.jrpShop_buy, message, "Purchase;Back")
end

local function ShowSelectMenu(eventStatus,pid,guiID,message)
	local listPrefix = "BACK\n"
	shopTable[pid].entries = {}

	if shopTable[pid].configPath ~= nil then
		for entryName,entryTable in pairs(shopTable[pid].configPath) do
			table.insert(shopTable[pid].entries,entryName)
			table.sort(shopTable[pid].entries)
			list = listPrefix .. table.concat(shopTable[pid].entries,"\n")
		end
		tes3mp.ListBox(pid,guiID,message,list)
	else
		tes3mp.MessageBox(pid, -1, "No items found in category.")
		ShowMainMenu(eventStatus,pid)
	end
end

local function GetPlayerGold(pid)
	local goldLoc = inventoryHelper.getItemIndex(Players[pid].data.inventory, "gold_001", -1)
	if goldLoc then
		return Players[pid].data.inventory[goldLoc].count
	else
		return 0
	end
end

local function RemoveGold(pid,amount)
	local goldLoc = inventoryHelper.getItemIndex(Players[pid].data.inventory, "gold_001", -1)	
	Players[pid].data.inventory[goldLoc].count = Players[pid].data.inventory[goldLoc].count - amount
	
	local itemref = {refId = "gold_001", count = amount, charge = -1}			
	Players[pid]:Save()
	Players[pid]:LoadItemChanges({itemref}, enumerations.inventory.REMOVE)	
end

local function ReturnOld(pid)
	if shopTable[pid].itemType == "equip" then
		if shopTable[pid].oldSelect ~= nil then
			tes3mp.LogMessage(2, "[jrpShop] " .. Players[pid].name .. " had item equipped, so giving old equip")
			Players[pid].data.equipment[shopTable[pid].itemSlot] = shopTable[pid].oldSelect
			tes3mp.EquipItem(pid, shopTable[pid].itemSlot, shopTable[pid].oldSelect["refId"], 1, -1, -1)
			tes3mp.SendEquipment(pid)
		else
			tes3mp.LogMessage(2, "[jrpShop] " .. Players[pid].name .. " had nothing equipped, so giving nothing")
			Players[pid].data.equipment[shopTable[pid].itemSlot] = nil
			tes3mp.SendEquipment(pid)
		end
		inventoryHelper.removeItem(Players[pid].data.inventory, shopTable[pid].refId, 1)
		tes3mp.SendInventoryChanges(pid)
		Players[pid]:LoadInventory()
		Players[pid]:LoadEquipment()
		toggleTVM(pid) -- just gonna toggle camera because i can't find the right way to remove equip in vanity mode
		toggleTVM(pid)
	elseif shopTable[pid].itemType == "hair" and shopTable[pid].oldSelect ~= nil then
		Players[pid].data.character.hair = shopTable[pid].oldSelect
		tes3mp.SetHair(pid, shopTable[pid].oldSelect)
		tes3mp.SetResetStats(pid, false)
		tes3mp.SendBaseInfo(pid)
		Players[pid]:LoadShapeshift()
	elseif shopTable[pid].itemType == "head" and shopTable[pid].oldSelect ~= nil then
		Players[pid].data.character.head = shopTable[pid].oldSelect
		tes3mp.SetHead(pid, shopTable[pid].oldSelect)
		tes3mp.SetResetStats(pid, false)
		tes3mp.SendBaseInfo(pid)
		Players[pid]:LoadShapeshift()
	end
end

local function CheckGUI(newStatus,pid,idGui,data)
	if idGui == guiHelper.ID.jrpShop_mainMenu then
		if tonumber(data) == 0 then -- change body
			ShowBodyMenu(eventStatus,pid)
		elseif tonumber(data) == 1 then -- buy armor
			ShowArmorMenu(eventStatus,pid)
		elseif tonumber(data) == 2 then -- buy clothing
			ShowClothingMenu(eventStatus,pid)
		elseif tonumber(data) == 3 then -- buy misc
			ClearTable(pid)
			shopTable[pid].itemType = "inv"
			shopTable[pid].configPath = jrpShop.config.misc
			ShowSelectMenu(eventStatus,pid,guiHelper.ID.jrpShop_select,"Miscellaneous")
		else
			toggleTVM(pid)
		end
	elseif idGui == guiHelper.ID.jrpShop_armorMenu then
		if tonumber(data) ==  0 then
			shopTable[pid].itemType = "equip"
			shopTable[pid].itemSlot = 0
			shopTable[pid].configPath = jrpShop.config.helmets
			ShowSelectMenu(eventStatus,pid,guiHelper.ID.jrpShop_select,"Headgear")
		elseif tonumber(data) == 1 then
			shopTable[pid].itemType = "equip"
			shopTable[pid].itemSlot = 1
			shopTable[pid].configPath = jrpShop.config.cuirass
			ShowSelectMenu(eventStatus,pid,guiHelper.ID.jrpShop_select,"Cuirass")
		elseif tonumber(data) == 2 then
			shopTable[pid].itemType = "equip"
			shopTable[pid].itemSlot = 2
			shopTable[pid].configPath = jrpShop.config.greaves
			ShowSelectMenu(eventStatus,pid,guiHelper.ID.jrpShop_select,"Greaves")
		elseif tonumber(data) ==  3 then
			shopTable[pid].itemType = "equip"
			shopTable[pid].itemSlot = 3
			shopTable[pid].configPath = jrpShop.config.lpauldrons
			ShowSelectMenu(eventStatus,pid,guiHelper.ID.jrpShop_select,"Left Pauldron")
		elseif tonumber(data) ==  4 then
			shopTable[pid].itemType = "equip"
			shopTable[pid].itemSlot = 4
			shopTable[pid].configPath = jrpShop.config.rpauldrons
			ShowSelectMenu(eventStatus,pid,guiHelper.ID.jrpShop_select,"Right Pauldron")
		elseif tonumber(data) ==  5 then -- where do bracers fit in here?
			shopTable[pid].itemType = "equip"
			shopTable[pid].itemSlot = 5
			shopTable[pid].configPath = jrpShop.config.lgauntlets
			ShowSelectMenu(eventStatus,pid,guiHelper.ID.jrpShop_select,"Left Gauntlet")
		elseif tonumber(data) ==  6 then
			shopTable[pid].itemType = "equip"
			shopTable[pid].itemSlot = 6
			shopTable[pid].configPath = jrpShop.config.rgauntlets
			ShowSelectMenu(eventStatus,pid,guiHelper.ID.jrpShop_select,"Right Gauntlet")
		elseif tonumber(data) ==  7 then
			shopTable[pid].itemType = "equip"
			shopTable[pid].itemSlot = 7
			shopTable[pid].configPath = jrpShop.config.boots
			ShowSelectMenu(eventStatus,pid,guiHelper.ID.jrpShop_select,"Boots")
		elseif tonumber(data) ==  8 then
			shopTable[pid].itemType = "equip"
			shopTable[pid].itemSlot = 17
			shopTable[pid].configPath = jrpShop.config.shields
			ShowSelectMenu(eventStatus,pid,guiHelper.ID.jrpShop_select,"Shields")
		else-- back
			ShowMainMenu(eventStatus,pid)
		end
	elseif idGui == guiHelper.ID.jrpShop_clothingMenu then
		if tonumber(data) == 0 then -- shirts
			shopTable[pid].itemType = "equip" -- how we'll assign new item to player
			shopTable[pid].itemSlot = 8
			shopTable[pid].configPath = jrpShop.config.shirts
			ShowSelectMenu(eventStatus,pid,guiHelper.ID.jrpShop_select,"Shirts")
		elseif tonumber(data) ==  1 then -- pants
			shopTable[pid].itemType = "equip"
			shopTable[pid].itemSlot = 9
			shopTable[pid].configPath = jrpShop.config.pants
			ShowSelectMenu(eventStatus,pid,guiHelper.ID.jrpShop_select,"Pants")
		elseif tonumber(data) ==  2 then -- robes
			shopTable[pid].itemType = "equip"
			shopTable[pid].itemSlot = 11
			shopTable[pid].configPath = jrpShop.config.robes
			ShowSelectMenu(eventStatus,pid,guiHelper.ID.jrpShop_select,"Robes")
		elseif tonumber(data) ==  3 then -- rgloves
			shopTable[pid].itemType = "equip"
			shopTable[pid].itemSlot = 6
			shopTable[pid].configPath = jrpShop.config.rgloves
			ShowSelectMenu(eventStatus,pid,guiHelper.ID.jrpShop_select,"Right Gloves")
		elseif tonumber(data) ==  4 then -- lgloves
			shopTable[pid].itemType = "equip"
			shopTable[pid].itemSlot = 5
			shopTable[pid].configPath = jrpShop.config.lgloves
			ShowSelectMenu(eventStatus,pid,guiHelper.ID.jrpShop_select,"Left Gloves")
		elseif tonumber(data) ==  5 then -- skirts
			shopTable[pid].itemType = "equip"
			shopTable[pid].itemSlot = 10
			shopTable[pid].configPath = jrpShop.config.skirts
			ShowSelectMenu(eventStatus,pid,guiHelper.ID.jrpShop_select,"Skirts")
		elseif tonumber(data) ==  6 then -- shoes
			shopTable[pid].itemType = "equip"
			shopTable[pid].itemSlot = 7
			shopTable[pid].configPath = jrpShop.config.shoes
			ShowSelectMenu(eventStatus,pid,guiHelper.ID.jrpShop_select,"Shoes")
		else-- back
			ShowMainMenu(eventStatus,pid)
		end
	elseif idGui == guiHelper.ID.jrpShop_bodyMenu then
		local playerRace = string.lower(Players[pid].data.character.race)
		if tonumber(data) == 0 then -- change hair
			shopTable[pid].itemType = "hair" -- how we'll assign new item to player
			shopTable[pid].configPath = jrpShop.config.hairs[playerRace]
			ShowSelectMenu(eventStatus,pid,guiHelper.ID.jrpShop_select,"Available Hairs")
		elseif tonumber(data) == 1 then -- change head
			shopTable[pid].itemType = "head" -- how we'll assign new item to player
			shopTable[pid].configPath = jrpShop.config.heads[playerRace]
			ShowSelectMenu(eventStatus,pid,guiHelper.ID.jrpShop_select,"Available Heads")
		elseif tonumber(data) == 2 then -- change height
			ShowHeightMenu(eventStatus,pid)
		elseif tonumber(data) == 3 then -- back
			ShowMainMenu(eventStatus,pid)
		end
	elseif idGui == guiHelper.ID.jrpShop_select then
		if tonumber(data) ~= 0 and tonumber(data) < 5000000 then -- big number for non-button selections
			tes3mp.LogMessage(2, tonumber(data))
			tes3mp.LogMessage(2, shopTable[pid].configPath[shopTable[pid].entries[tonumber(data)]].id)
			shopTable[pid].refId = shopTable[pid].configPath[shopTable[pid].entries[tonumber(data)]].id -- set to reference in buy menu
			shopTable[pid].itemName = shopTable[pid].entries[tonumber(data)]
			tes3mp.LogMessage(2,"[jrpShop] " .. Players[pid].name .. " selected " .. shopTable[pid].refId .. " for viewing.")
			if shopTable[pid].itemType == "equip" then
				if Players[pid].data.equipment[shopTable[pid].itemSlot] ~= nil or Players[pid].data.equipment[shopTable[pid].itemSlot] ~= {} or Players[pid].data.equipment[shopTable[pid].itemSlot] ~= "" then-- copy old equipped item to return later. idk the exact format
					shopTable[pid].oldSelect = Players[pid].data.equipment[shopTable[pid].itemSlot]
				else
					shopTable[pid].oldSelect = nil
				end
				shopTable[pid].itemCost = shopTable[pid].configPath[shopTable[pid].entries[tonumber(data)]].cost
				tes3mp.EquipItem(pid, shopTable[pid].itemSlot, shopTable[pid].refId, 1, -1, -1)
				tes3mp.SendEquipment(pid)
			elseif shopTable[pid].itemType == "hair" then
				shopTable[pid].oldSelect = Players[pid].data.character.hair
				shopTable[pid].itemCost = jrpShop.config.hairs["cost"]
				Players[pid].data.character.hair = shopTable[pid].refId
				tes3mp.SetHair(pid, shopTable[pid].refId)
				tes3mp.SetResetStats(pid, false)
				tes3mp.SendBaseInfo(pid)
				Players[pid]:LoadShapeshift()
			elseif shopTable[pid].itemType == "head" then
				shopTable[pid].oldSelect = Players[pid].data.character.head
				shopTable[pid].itemCost = jrpShop.config.heads["cost"]
				Players[pid].data.character.head = shopTable[pid].refId
				tes3mp.SetHead(pid, shopTable[pid].refId)
				tes3mp.SetResetStats(pid, false)
				tes3mp.SendBaseInfo(pid)
				Players[pid]:LoadShapeshift()
			elseif shopTable[pid].itemType == "inv" then
				-- continue on
			else
				tes3mp.LogMessage(2,"[jrpShop] " .. Players[pid].name .. " somehow got a nil shopTable itemType. Sending back to main menu.")
				ShowMainMenu(eventStatus,pid)
				tes3mp.MessageBox(pid, -1, "Sorry! An error occurred. Try again.")
			end
			ShowBuyMenu(eventStatus,pid)
		else
			toggleTVM(pid) -- no item input given. kicking out of vanity mode
		end
	elseif idGui == guiHelper.ID.jrpShop_buy then
		if tonumber(data) == 0 then -- purchase
			local pgold = GetPlayerGold(pid)
			if pgold >= shopTable[pid].itemCost then
				tes3mp.LogMessage(2,"[jrpShop] " .. Players[pid].name .. " purchased " .. shopTable[pid].refId)
				RemoveGold(pid,shopTable[pid].itemCost)
				if shopTable[pid].itemType == "inv" then
					inventoryHelper.addItem(Players[pid].data.inventory, shopTable[pid].refId, 1, -1, -1, "") -- put in inv
					tes3mp.ClearInventoryChanges(pid) -- there's gotta be a cleaner way to do this
					tes3mp.SetInventoryChangesAction(pid, enumerations.inventory.ADD)
					tes3mp.AddItemChange(pid, shopTable[pid].refId, 1, -1, -1, "")
					tes3mp.SendInventoryChanges(pid)
				else
					tes3mp.SendEquipment(pid) -- equip it
				end
			else
				tes3mp.LogMessage(2,"[jrpShop] " .. Players[pid].name .. " could not afford to purchase " .. shopTable[pid].refId)
				tes3mp.MessageBox(pid, -1, "You can't afford this item!")
				ReturnOld(pid) -- because they didn't actually buy it
				ShowMainMenu(eventStatus,pid)
			end
		else -- return old
			ReturnOld(pid)
		end
		ClearTable(pid)
		ShowMainMenu(eventStatus,pid)
	elseif idGui == guiHelper.ID.jrpShop_heightMenu then
		if tonumber(data) == 0 then -- lower
			if Players[pid].data.shapeshift.scale <= 0.90 then
				tes3mp.MessageBox(pid, -1, "Too short! Ask an admin for further scale adjustment.")
			else
				Players[pid]:SetScale(Players[pid].data.shapeshift.scale - 0.02)
				Players[pid]:LoadShapeshift()
			end
			ShowHeightMenu(eventStatus,pid)
		elseif tonumber(data) == 1 then -- back
			ShowBodyMenu(eventStatus,pid)
		elseif tonumber(data) == 2 then -- raise
			if Players[pid].data.shapeshift.scale >= 1.10 then
				tes3mp.MessageBox(pid, -1, "Too tall! Ask an admin for further scale adjustment.")
			else
				Players[pid]:SetScale(Players[pid].data.shapeshift.scale + 0.02)
				Players[pid]:LoadShapeshift()
			end
			ShowHeightMenu(eventStatus,pid)
		end
	end
end

local function ChatListener(pid,cmd)
	if cmd[1] == "shop" then
		ShowMainMenu(eventStatus,pid)
		toggleTVM(pid)
	end
end

customEventHooks.registerValidator("OnGUIAction", CheckGUI)
customEventHooks.registerHandler("OnPlayerAuthentified", ClearTable)
customEventHooks.registerHandler("OnPlayerDisconnect", ClearTable)
customCommandHooks.registerCommand("shop", ChatListener)
