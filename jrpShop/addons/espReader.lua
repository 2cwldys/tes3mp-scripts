-- jrpShop addon by Malic for espParser
-- Only meant for addon creation, should be used locally
-- prints lua tables to put into addon file

require("custom.struct")
require("custom.espParser")

local totalTable = {}
totalTable.shirts = {}
totalTable.skirts = {}
totalTable.pants = {}
totalTable.robes = {}
totalTable.rgloves = {}
totalTable.lgloves = {}
totalTable.shoes = {}
totalTable.heads = {}
totalTable.hairs = {}
totalTable.helmets = {}
totalTable.cuirass = {}
totalTable.greaves = {}
totalTable.lpauldrons = {}
totalTable.rpauldrons = {}
totalTable.boots = {}
totalTable.shields = {}
totalTable.lgauntlets = {}
totalTable.rgauntlets = {}
totalTable.lbracers = {}
totalTable.rbracers = {}

for _, file in pairs(espParser.files) do    
	for filename, records in pairs(espParser.rawFiles) do
		tes3mp.LogMessage(2, "Found file " .. filename)
		for _,record in pairs(records) do
			clothing = {}
			bodypart = {}
			armor = {}
			if record.name == "CLOT" then
				for _,subrecord in pairs(record.subRecords) do
					if subrecord.name == "NAME" then
						clothing.id = subrecord.data
					elseif subrecord.name == "FNAM" then
						clothing.name= subrecord.data
					elseif subrecord.name == "CTDT" then
						clothing.type = struct.unpack( "i", string.sub(subrecord.data, 0, 4) )
						clothing.value = struct.unpack( "h", string.sub(subrecord.data, 9, 10) )
					end
				end
			elseif record.name == "BODY" then
				for _,subrecord in pairs(record.subRecords) do
					if subrecord.name == "NAME" then
						bodypart.id = subrecord.data
					elseif subrecord.name == "BYDT" then
						bodypart.part = struct.unpack("h", string.sub(subrecord.data, 0, 4))
						bodypart.type = struct.unpack("b", string.sub(subrecord.data, 4, 8))
					end
				end
			elseif record.name == "ARMO" then
				for _,subrecord in pairs(record.subRecords) do
					if subrecord.name == "NAME" then
						armor.id = subrecord.data
					elseif subrecord.name == "FNAM" then
						armor.name = subrecord.data
					elseif subrecord.name == "AODT" then
						armor.type = struct.unpack("h", string.sub(subrecord.data, 0, 4))
						armor.value = struct.unpack("i", string.sub(subrecord.data, 9, 12))
					end
				end
			end
			
			if clothing.id ~= nil and clothing.name ~= nil and clothing.value ~= nil and clothing.type ~= nil then
				local text = '[\"' .. clothing.name:gsub('[^%w_ ]','') .. '\"] = {[\"id\"] = \"' .. clothing.id:gsub('[^%w_ ]','')  .. '\", [\"cost\"] = ' .. clothing.value .. '},'
				if clothing.type == 0 then
					table.insert(totalTable.pants,text)
				elseif clothing.type == 1 then
					table.insert(totalTable.shoes,text)
				elseif clothing.type == 2 then
					table.insert(totalTable.shirts,text)
				elseif clothing.type == 4 then
					table.insert(totalTable.robes,text)
				elseif clothing.type == 5 then
					table.insert(totalTable.rgloves,text)
				elseif clothing.type == 6 then
					table.insert(totalTable.lgloves,text)
				elseif clothing.type == 7 then
					table.insert(totalTable.skirts,text)
				end
			elseif bodypart.id ~= nil and bodypart.part ~= nil then
				local text = '[\"' .. bodypart.id:gsub('[^%w_ ]','') .. '\"] = {[\"id\"] = \"' .. bodypart.id:gsub('[^%w_ ]','')  .. '\"},'
				if bodypart.part == 0 and bodypart.type == 0 then
					table.insert(totalTable.heads,text)
				elseif bodypart.part == 1 and bodypart.type == 0 then
					table.insert(totalTable.hairs,text)
				end
			elseif armor.id ~= nil and armor.name ~= nil and armor.value ~= nil then
				local text = '[\"' .. armor.name:gsub('[^%w_ ]','') .. '\"] = {[\"id\"] = \"' .. armor.id:gsub('[^%w_ ]','')  .. '\", [\"cost\"] = ' .. armor.value .. '},'
				if armor.type == 0 then
					table.insert(totalTable.helmets,text)
				elseif armor.type == 1 then
					table.insert(totalTable.cuirass,text)
				elseif armor.type == 2 then
					table.insert(totalTable.lpauldrons,text)
				elseif armor.type == 3 then
					table.insert(totalTable.rpauldrons,text)
				elseif armor.type == 4 then
					table.insert(totalTable.greaves,text)
				elseif armor.type == 5 then
					table.insert(totalTable.boots,text)
				elseif armor.type == 6 then
					table.insert(totalTable.lgauntlets,text)
				elseif armor.type == 7 then
					table.insert(totalTable.rgauntlets,text)
				elseif armor.type == 8 then
					table.insert(totalTable.shields,text)
				elseif armor.type == 9 then
					table.insert(totalTable.lbracers,text)
				elseif armor.type == 10 then
					table.insert(totalTable.rbracers,text)
				end
			end
		end	
	end
end

for categoryName,category in pairs(totalTable) do
	print("\n" .. categoryName .. ":")
	for _,entry in pairs(category) do
		print(entry)
	end
end
