## jrpShop (for 0.7)
-	A completely configurable clothing shop and head/hair/height changer for players
-	Requires DataManager
-	Under GPLv3

![preview](preview.jpg)
![preview](preview2.jpg)

### Installation
-	Install [DataManager](https://github.com/tes3mp-scripts/DataManager) by uramer
-	Add main.lua and defaultConfig.lua to scripts/custom/jrpShop/
-	Add to customScripts.lua under DataManagers line:
		`require("custom.jrpShop.main")`

### Commands
-   **/appearance -** Show the menu

### To-do
-	Fix using ESC button from ListBox locking player in vanity mode (if possible)
-	A *whoooole* lotta code cleaning
-	Find out how bracers relate to itemSlots
