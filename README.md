A collection of TES3MP scripts by myself, including a fork directory of some other scripts I use on JRP Roleplay.

### Frameworks
-	**CellInserter** - A framework to easily add or remove objects from the world after cell resets

### Features
-	**balmoraFixMe** - Adds command to teleport to Balmora, for really stuck situations
-	**cellManualBackup** - Adds command to backup the current cell json into a custom directory
-	**easyFind** - Simple GUI to print every players location
-	**jrpAnim** - Simple GUI to play player animations without commands
-	**jrpShop** - Customizable GUI for players to modify their character and buy clothing
-	**jrpChat** - Rewrite of the chat system based around roleplay, with language and nickname support
-	**jrpCompanions** - Working, realistically usable companion/follower system (highly experimental)
-	**jrpRolls** - d100 rolling system with player-set buffs and debuffs
-	**jrpStatus** - Players can set in-game viewable biographies, appearance descriptions, and other character sheet esque traits
-	**jrpTravel** - Alternative to multiple traveling systems, with teleporters between Telvanni settlements and all Mage Guild locations accessible from each guild transporter
-	**noticeBoard** - Public book players can read/write entries into8
-	**portableBedroll** - Bedrolls can be picked up like an item and still slept on
-	**questCounter** - Percentage tracker of how many quests a player has done in each region and faction
-	**racialPerks** - Racial bonuses made for JRP
-	**setSex** - Command to change a players sex without logging off
-	**simpleRevive** - Activating a dead player revives them with a fraction of their health back.
-	**xCard** - Player safety tool for roleplay

### Fixes
-	**antiMainQuest** - Disables accessing some main quest places
-	**jrpFixes** - Simple script to add common quest/bug fixes
-	**speechAntiCrash** - Prevents server from crashing if custom race uses speech command


### Mod Support
-	**Wizard Hats** - Requires mod by Daduke. Replaces items with custom records

### Forks and Mirrors
-	**cursedItemsFix** - by Learwolf, replaces cursed items with original after use
-	**decorateHelp** - by Atkana, modified for 0.7
-	**kanaFurniture** - by Atkana, modified for 0.7
-	**kanaHousing** - by Atkana, modified for 0.7
-	**kanaBank** - by Atkana
-	**noteWriting** - by boyos999
-	**noteWritingPlus** - Modified noteWriting by Wishbone, Nac and myself. Adds GUI, scrolls, and books
-	**starterEquipment** - by Boyos999, changed gold amount.
