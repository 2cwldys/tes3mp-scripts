## jrpChat (for 0.7)
-	A roleplay based chat overhaul
-	All chat is local by default except for OOC
-	Supports nicknames and languages
-	Under GPLv3

![preview](preview.jpg)

### Installation
-	Add jrpChat.lua to scripts/custom/
-	Add to customScripts.lua:
		`require("custom.jrpChat")`
-	Add jrpChat_config.json to /data/custom/

### Commands
-   **no command -** Speak to whoever is in the cell.
-	**// text -** Use global OOC chat
-	**/// text -** Use local OOC chat (to everybody in your current cell)
-	**/me does a dance -** Perform an action. Example: *[ACTION] JohnSmith does a dance*
-	**/nickname text -** Sets your nickname
-	**/nickname -** Resets your nickname
-	**!lang hello -** Send a local message in another language

### To-do
-   Tidy this up, remove redundant functions
-	Add radius of hearing, whisper/shout commands
