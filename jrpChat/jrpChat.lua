-- jrpChat for tes3mp 0.7-prerelease. created by malic for JRP Roleplay
-- script that revamps how chat looks and feels, inspired by roleplay-chat-essentials
-- under GPLv3

local jrpChat = {}
jrpChat.config = jsonInterface.load("custom/jrpChat_config.json")

local function resetNickname(pid)
	Players[pid].data.customVariables.jrpChatName = Players[pid].name
	Players[pid]:Save()
	tes3mp.LogMessage(2, "[jrpChat] " .. Players[pid].name .. "'s nickname has been reset.")
end

local function setNickname(pid,cmd)
	if cmd[2] ~= nil then
		Players[pid].data.customVariables.jrpChatName = table.concat(cmd, " ", 2)
		Players[pid]:Save()
		tes3mp.LogMessage(2, Players[pid].accountName .. " set their nickname to " .. Players[pid].data.customVariables.jrpChatName)
		Players[pid]:Message(color.SlateGray .. "Your nickname has been set to " .. Players[pid].data.customVariables.jrpChatName .. color.Default .. "\n")
		return
	else
		resetNickname(pid)
	end
end

-- nickname seems to crash after a while? lets make sure the variable exists
local function nicknameGuarantee(pid)
	if Players[pid].data.customVariables.jrpChatName == nil then
		tes3mp.LogMessage(2, "[jrpChat] " .. Players[pid].name .. " found with jrpChatName undefined. Setting to their regular name.")
		Players[pid].data.customVariables.jrpChatName = Players[pid].name
		Players[pid]:Save()
	end
end

local function OnPlayerSendMessage(eventStatus, pid, message)
    local cellDescription = tes3mp.GetCell(pid)
    nicknameGuarantee(pid)

	if message:sub(1, 1) == '/' then
		return
	elseif message:sub(1, 1) == '!' then
		langMsg = (message:sub(2, #message)):split(" ")
		langAvailable = {}
		foundLang = false
		for langCmd,langName in pairs(jrpChat.config.languages) do
			table.insert(langAvailable, langCmd)
			if langMsg[1] == langCmd then
				selectedLang = langName
				foundLang = true
			end
		end
		
		if foundLang == false then
			table.sort(langAvailable)
			Players[pid]:Message(color.SlateGray .. "Languages: " .. table.concat(langAvailable,", ") .. color.Default .. "\n")
			return customEventHooks.makeEventStatus(false, nil)
		end
		
		langText = color.Gray .. " (in " .. selectedLang .. ")"
	else
		langText = ""
	end
    
    if Players[pid].name == Players[pid].data.customVariables.jrpChatName then
		playerName = color.Beige .. Players[pid].name
	else
		playerName = color.SlateGray .. Players[pid].data.customVariables.jrpChatName
    end
	
	if logicHandler.IsCellLoaded(cellDescription) then
		if message:sub(1, 1) == '!' then
			message = tableHelper.concatenateFromIndex(langMsg, 2)
		end
		local chatText = playerName .. langText .. ": " .. color.Default .. message .. "\n"
		for index, visitorPid in pairs(LoadedCells[cellDescription].visitors) do
			tes3mp.SendMessage(visitorPid, chatText, false)
		end
	end
	return customEventHooks.makeEventStatus(false, nil)
end


local function globalOOC(pid,cmd)
	local message = color.Orange .. "[OOC] " .. color.Default .. Players[pid].name .. " (" .. pid .. ")" .. ": " .. color.Gray
	message = message .. tableHelper.concatenateFromIndex(cmd, 2) .. "\n"
	tes3mp.SendMessage(pid, message, true)
end

local function localOOC(pid,cmd)
	local cellDescription = Players[pid].data.location.cell

	if logicHandler.IsCellLoaded(cellDescription) == true then
		for index, visitorPid in pairs(LoadedCells[cellDescription].visitors) do
			if Players[pid].name == Players[pid].data.customVariables.jrpChatName then
				local message = color.RosyBrown .. "[LOOC] " .. color.Default .. Players[pid].name .. " (" .. pid .. ")" .. ": " .. color.Gray
				message = message .. tableHelper.concatenateFromIndex(cmd, 2) .. "\n"
				tes3mp.SendMessage(visitorPid, message, false)
			else
				local message = color.RosyBrown .. "[LOOC] " .. color.Default .. Players[pid].data.customVariables.jrpChatName .. " (" .. pid .. ")" .. ": "  .. color.Gray
				message = message .. tableHelper.concatenateFromIndex(cmd, 2) .. "\n"
				tes3mp.SendMessage(visitorPid, message, false)
			end
		end
	end
end

local function actionCommand(pid,cmd)
	local cellDescription = Players[pid].data.location.cell

	if logicHandler.IsCellLoaded(cellDescription) == true then
		for index, visitorPid in pairs(LoadedCells[cellDescription].visitors) do
			if Players[pid].name == Players[pid].data.customVariables.jrpChatName then
				local message = color.Red .. "[ACTION] " .. color.Default .. Players[pid].name .. " "
				message = message .. tableHelper.concatenateFromIndex(cmd, 2) .. "\n"
				tes3mp.SendMessage(visitorPid, message, false)
			else
				local message = color.Red .. "[ACTION] " .. color.Default .. Players[pid].data.customVariables.jrpChatName .. " "
				message = message .. tableHelper.concatenateFromIndex(cmd, 2) .. "\n"
				tes3mp.SendMessage(visitorPid, message, false)
			end
		end
	end
end

local function OnPlayerFinishLogin(eventStatus,pid)
	resetNickname(pid)
end

local function ChatListener(pid, cmd)
	--nickname commands
	if cmd[1] == "nickname" and cmd[2] ~= nil then
		setNickname(pid,cmd)
	elseif cmd[1] == "nickname" and cmd[2] == nil then
		resetNickname(pid)
		Players[pid]:Message(color.SlateGray .. "Your nickname has been reset.\n" .. color.Default)
		return
	-- generic chat commands
	elseif cmd[1] == "/" then
		globalOOC(pid,cmd)
	elseif cmd[1] == "//" then
		localOOC(pid,cmd)
	elseif cmd[1] == "me" then
		actionCommand(pid,cmd)
	--greentext toggle
	elseif cmd[1] == "gt" and jrpChat.config.chat.disable_greentext == true then
		return
	-- disable /l (replaced by /// for LOOC)
	elseif cmd[1] == "l" then
		return
	end
end

customEventHooks.registerHandler("OnPlayerFinishLogin", OnPlayerFinishLogin)
customEventHooks.registerValidator("OnPlayerSendMessage", OnPlayerSendMessage)
customCommandHooks.registerCommand("nickname", ChatListener)
customCommandHooks.registerCommand("/", ChatListener)
customCommandHooks.registerCommand("//", ChatListener)
customCommandHooks.registerCommand("me", ChatListener)
customCommandHooks.registerCommand("gt", ChatListener)
customCommandHooks.registerCommand("l", ChatListener)
